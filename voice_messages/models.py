from django.db import models
from accounts.models import Account
from devices.models import Device
from django.db.models.signals import post_save
from django.dispatch import receiver


class VoiceMessages(models.Model):
    """
        Base class for voice records.
    """
    from_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='from_account')
    to_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='to_account')
    from_device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='from_device')
    to_device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='to_device')
    is_sent = models.BooleanField(default=False)
    sent_date = models.DateTimeField(null=True, blank=True)
    is_read = models.BooleanField(default=False)
    read_date = models.DateTimeField(null=True, blank=True)
    bucket_name = models.CharField(max_length=30, blank=False)
    bucket_key = models.CharField(max_length=30, blank=False)
    filename = models.CharField(max_length=30, blank=False)

    def __str__(self):
        return 'Voice message from ' + self.from_account.__str__() + ' to ' + self.to_account.__str__()

    @property
    def bucket_url(self):
        return "http://s3.eu-central-1.amazonaws.com/" + self.bucket_name + "/" + self.bucket_key + "/" + self.filename


class VoiceMessageOptionals(models.Model):
    """
        Base class for voice record optionals.
    """
    voice_message = models.OneToOneField(VoiceMessages, on_delete=models.CASCADE,
                                         related_name='voice_message_optionals')
    is_set_time = models.BooleanField(default=False)
    set_date = models.DateTimeField(null=True, blank=True)
    expiration_date = models.DateTimeField(null=True, blank=True)
    will_repeat = models.BooleanField(default=False)
    will_repeat_by_time = models.BooleanField(default=False)
    repeat_time = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return 'Voice message optional fields for ' + self.voice_message.__str__()


@receiver(post_save, sender=VoiceMessages)
def create_voice_message_optionals(sender, instance, created, **kwargs):
    if created:
        voice_message_optionals = VoiceMessageOptionals.objects.create(voice_message=instance)
        voice_message_optionals.save()
