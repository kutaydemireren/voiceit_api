from rest_framework.status import HTTP_406_NOT_ACCEPTABLE

bucket_name_is_empty = {'error_message': 'Empty bucket name',
                        'error_status': HTTP_406_NOT_ACCEPTABLE}

bucket_name_is_invalid = {'error_message': 'Invalid bucket name',
                          'error_status': HTTP_406_NOT_ACCEPTABLE}
