# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-05 20:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('voice_messages', '0002_auto_20161104_1853'),
    ]

    operations = [
        migrations.AddField(
            model_name='voicemessages',
            name='filename',
            field=models.CharField(default='test.wav', max_length=30),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='voicemessageoptionals',
            name='voice_message',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='voice_message_optionals', to='voice_messages.VoiceMessages'),
        ),
    ]
