from voice_messages.models import VoiceMessages, VoiceMessageOptionals
from datetime import datetime, timedelta

__SUCCESS_RETURN = 'Success'
__FAILURE_RETURN = 'Failure'


def postpone(message_id, postpone_time):
    try:
        voice_message = VoiceMessages.objects.get(pk=message_id)
        voice_message_optional = voice_message.voice_message_optionals

        current_time = (datetime.now() - timedelta(hours=3))
        time_to_add = __construct_timedelta(postpone_time=postpone_time)
        set_date = current_time + time_to_add

        voice_message_optional.set_date = set_date.strftime("%Y-%m-%d %H:%M:%S")
        voice_message_optional.is_set_time = True
        voice_message_optional.save()
        return __SUCCESS_RETURN
    except Exception as ex:
        return __FAILURE_RETURN + ' ' + ex.__str__()


def __construct_timedelta(postpone_time):
    hr, mins, sec = map(float, postpone_time.split(':'))
    return timedelta(hours=hr, minutes=mins, seconds=sec)


def read(message_id):
    try:
        voice_message = VoiceMessages.objects.get(pk=message_id)

        voice_message.read_date = datetime.now()
        voice_message.is_read = True
        voice_message.save()
        return __SUCCESS_RETURN
    except Exception as ex:
        return __FAILURE_RETURN + ' ' + ex.__str__()


def repeat_same_message(optional_params, message_id):
    try:
        voice_message = VoiceMessages.objects.get(pk=message_id)
        voice_message_optionals = voice_message.voice_message_optionals

        new_voice_message = __create_voice_message(voice_message)
        new_voice_message_optionals = new_voice_message.voice_message_optionals

        __set_voice_message_optionals(optional_params, new_voice_message_optionals.id)

        return VoiceMessages.objects.get(pk=new_voice_message.id)
    except Exception as ex:
        return __FAILURE_RETURN + ' ' + ex.__str__()


def __set_voice_message_optionals(optional_params, voice_message_optionals_id):
    if type(voice_message_optionals_id) is not int:
        voice_message_optionals_id = int(voice_message_optionals_id)

    voice_message_optionals = VoiceMessageOptionals.objects.get(pk=voice_message_optionals_id)
    set_date = optional_params['set_date']
    if set_date is None:
        voice_message_optionals.is_set_time = False

        # expiration date must be None no matter what, if set date is None
        voice_message_optionals.expiration_date = None
    else:
        voice_message_optionals.is_set_time = True

        expiration_date = optional_params['expiration_date']
        expiration_date_split = expiration_date.split('_')
        voice_message_optionals.expiration_date = expiration_date_split[0] + ' ' + expiration_date_split[1]

        set_date_split = set_date.split('_')
        voice_message_optionals.set_date = set_date_split[0] + ' ' + set_date_split[1]
    repeat_time = optional_params['repeat_time']
    if repeat_time is None:
        voice_message_optionals.will_repeat = False
        voice_message_optionals.will_repeat_by_time = False
    else:
        voice_message_optionals.will_repeat = True
        voice_message_optionals.will_repeat_by_time = True

        repeat_time_split = repeat_time.split('_')
        voice_message_optionals.repeat_time = repeat_time_split[0] + ' ' + repeat_time_split[1]

    voice_message_optionals.save()


def __create_voice_message(voice_message):
    return VoiceMessages.objects.create(from_account=voice_message.from_account,
                                        to_account=voice_message.to_account,
                                        from_device=voice_message.from_device,
                                        to_device=voice_message.to_device,
                                        is_sent=True,
                                        sent_date=datetime.now(),
                                        is_read=False,
                                        read_date=None,
                                        bucket_name=voice_message.bucket_name,
                                        bucket_key=voice_message.bucket_key,
                                        filename=voice_message.filename)
