from django.conf.urls import url, include
from voice_messages import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'voice-messages', views.VoiceMessagesViewSet, 'voicemessages')

urlpatterns = [
    url(r'^upload/(?P<dirname>[^/]+)$', views.FileUploadView.as_view()),
    url(r'^voice-messages/pendings/(?P<account_id>\d+)',  views.PendingVoiceMessageList.as_view()),
    url(r'^voice-messages/inbox/(?P<account_id>\d+)$',  views.InboxList.as_view()),
    url(r'^voice-messages/outbox/(?P<account_id>\d+)$',  views.OutboxList.as_view()),
    url(r'^voice-messages/with-options',  views.VoiceMessageCreateView.as_view()),
    url(r'^voice-messages/(?P<message_id>\d+)/postpone/(?P<postpone_time>.+)',
        views.VoiceMessagePostponeCommandView.as_view()),
    url(r'^voice-messages/(?P<message_id>\d+)/set-read', views.VoiceMessageReadCommandView.as_view()),
    url(r'^voice-messages/(?P<message_id>\d+)/repeat-message', views.VoiceMessageRepeatSameMessageCommandView.as_view()),
]

routeList = (
    (r'voice-messages', views.VoiceMessagesViewSet, 'voicemessages'),
    (r'voice-messages-optionals', views.VoiceMessageOptionalsViewSet, 'voicemessageoptionals'),
)
