from rest_framework import serializers

from accounts.models import Account
from accounts.serializers import AccountSerializer
from devices.serializers import DeviceSerializers
from voice_messages.models import VoiceMessages, VoiceMessageOptionals


class VoiceMessageOptionalSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VoiceMessageOptionals
        fields = ('id', 'url', 'is_set_time', 'set_date', 'expiration_date',
                  'will_repeat', 'will_repeat_by_time', 'repeat_time')


class VoiceMessageSerializers(serializers.HyperlinkedModelSerializer):
    from_account = AccountSerializer(many=False, read_only=True)
    from_account_id = serializers.IntegerField(write_only=True)
    to_account = AccountSerializer(many=False, read_only=True)
    to_account_id = serializers.IntegerField(write_only=True)
    to_device = DeviceSerializers(many=False, read_only=True)
    to_device_id = serializers.IntegerField(write_only=True)
    from_device = DeviceSerializers(many=False, read_only=True)
    from_device_id = serializers.IntegerField(write_only=True)
    voice_message_optionals = VoiceMessageOptionalSerializers(many=False, read_only=True)
    is_sent = serializers.BooleanField(read_only=True)
    sent_date = serializers.DateTimeField(read_only=True)
    is_read = serializers.BooleanField(read_only=True)
    read_date = serializers.DateTimeField(read_only=True)
    bucket_name = serializers.CharField(read_only=True)
    bucket_key = serializers.CharField(read_only=True)
    filename = serializers.CharField(read_only=True)

    class Meta:
        model = VoiceMessages
        fields = ('id', 'url', 'from_account_id', 'from_account', 'to_account_id', 'to_account', 'from_device_id',
                  'from_device', 'to_device_id', 'to_device', 'is_sent', 'sent_date', 'is_read', 'read_date',
                  'bucket_name', 'bucket_key', 'filename', 'bucket_url', 'voice_message_optionals')

    def create(self, validated_data):
        f_acc = Account.objects.get(pk=validated_data['from_account_id'])
        t_acc = Account.objects.get(pk=validated_data['to_account_id'])
        voice_message = VoiceMessages.objects.create(from_account=f_acc,
                                                     to_account=t_acc,
                                                     is_sent=validated_data['is_sent'],
                                                     sent_date=validated_data['sent_date'],
                                                     is_read=validated_data['is_read'],
                                                     read_date=validated_data['read_date'],
                                                     bucket_name=validated_data['bucket_name'],
                                                     bucket_key=validated_data['bucket_key'],
                                                     filename=validated_data['filename'])

        return voice_message


class PendingVoiceMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = VoiceMessages
        fields = ('id', 'from_account_id', 'to_account_id', 'bucket_url',)
