from rest_framework import viewsets, permissions, status
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from voiceit.AWS import AWSClient
from voice_messages.models import VoiceMessages, VoiceMessageOptionals
from voice_messages import serializers
from voice_messages import view_manager

_FILE_FIELD_NAME = 'file'


class FileUploadView(APIView):
    """
    Uploads file to S3
    """
    parser_classes = (MultiPartParser, FormParser,)
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, dirname, format=None):
        """
        Puts uploaded file to Amazon S3 bucket.

        filename should end with .wav, checking for the extension. Other file specification does not matter.

        **param request**
            * From account id should be placed into the body with the key 'from_account_id' as integer,
            * To account ids should be placed into the body with the key 'to_account_ids' as list, e.g. [1, 2, 3, 4],
            * To device id should be placed into the body with the key 'to_device_id',
                * if request is coming from board, request should not have this area.
            * File should be placed into the body with the key 'file' as file raw data, \
            * From device id should be placed into the body with the key 'from_device_id'
                * board should provide this and not phone

        **param dirname** dirname in the AWS should be included. \

        Available dirnames are:
            * voice-messages \

        **return** the url where the file is located at
        """
        try:
            file_obj = request.data[_FILE_FIELD_NAME]

            aws_client = AWSClient()
            is_success, response = aws_client.put(file_obj, request.data, dirname)

            if is_success:
                return Response(response, status=status.HTTP_201_CREATED)
            else:
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response('Something went wrong (Possible issue with the body of the content).' +
                            'error: ' + ex.__str__(),
                            status=status.HTTP_400_BAD_REQUEST)


class VoiceMessagesViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.

    Just for dummy data creation.

    For the actual creation of voice message please refer to /upload endpoint(s)  or /voice-messages/with-options.
    """
    queryset = VoiceMessages.objects.all().order_by('id')
    serializer_class = serializers.VoiceMessageSerializers
    permission_classes = (permissions.IsAuthenticated,)


class VoiceMessageCreateView(APIView):
    """
    Provides `create` action for voice messages
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        """
        Create a new voice message with the given file.

        filename should end with .wav, checking for the extension. Other file specification does not matter.

        **param parameters** Parameters are placed after the endpoint starting with ? and separated with &

        Ex: /voice-messages/with-options?set_date=2016-11-02_01:02:00&expiration_date=2016-11-02_02:02:00

        4 optional parameters are available:

            * set_date  2016-11-02 or 2016-11-02 01:02:00
            * expiration_date  2016-11-02 or 2016-11-02 01:02:00
                * set_date and expiration_date should be provided together.
            * repeat_time  1-hour or 14-minute or 3-days
                * be careful about being single and not multiple (e.g. not minute's', instead use minute)

        **request body**
            * From account id should be placed into the body with the key 'from_account_id' as integer,
            * To account ids should be placed into the body with the key 'to_account_ids' as list, e.g. [1, 2, 3, 4],
            * To device id should be placed into the body with the key 'to_device_id',
                * if request is coming from board, request should not have this area.
            * File should be placed into the body with the key 'file' as file raw data, \

        **return** the url where the file is located at
        """
        set_date = request.GET.get('set_date', None)
        expiration_date = request.GET.get('expiration_date', None)
        repeat_time = request.GET.get('repeat_time', None)

        optional_params = {
            'set_date': set_date,
            'expiration_date': expiration_date,
            'repeat_time': repeat_time,
        }

        try:
            file_obj = request.data[_FILE_FIELD_NAME]

            aws_client = AWSClient()
            is_success, response = aws_client.put_with_optionals(file_obj, request.data, optional_params)

            if is_success:
                return Response(response, status=status.HTTP_201_CREATED)
            else:
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response('Something went wrong (Possible issue with the body of the content). ' +
                            'Error: ' + ex.__str__(),
                            status=status.HTTP_400_BAD_REQUEST)


class VoiceMessagePostponeCommandView(APIView):
    """
    Provides `update` action for voice messages
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, message_id, postpone_time):
        """
        Postpone the given message for amount of time specified.

        **param parameters** Parameters are placed after the endpoint starting with ? and separated with &

        Ex: voice-messages/3/postpone/01:02:03

        2 optional parameters are available:

            * message_id  3, 4, 5 etc. - whatever the id of the voice message
            * postpone_time  01:02:03 - HH:MM:SS
        """
        try:
            response = view_manager.postpone(message_id=message_id, postpone_time=postpone_time)

            return Response(response, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response('Something went wrong (Possible issue with the body of the content). ' +
                            'Error: ' + ex.__str__(),
                            status=status.HTTP_400_BAD_REQUEST)


class VoiceMessageReadCommandView(APIView):
    """
    Marks the given message as read.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, message_id):
        try:
            response = view_manager.read(message_id=message_id)

            return Response(response, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response('Something went wrong (Possible issue with the body of the content). ' +
                            'Error: ' + ex.__str__(),
                            status=status.HTTP_400_BAD_REQUEST)


class VoiceMessageRepeatSameMessageCommandView(APIView):
    """
    Repeats the message, i.e. create a new voice message that has same values with the given voice message
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, message_id):
        set_date = request.GET.get('set_date', None)
        expiration_date = request.GET.get('expiration_date', None)
        repeat_time = request.GET.get('repeat_time', None)

        optional_params = {
            'set_date': set_date,
            'expiration_date': expiration_date,
            'repeat_time': repeat_time,
        }
        try:
            repeated_voice_message = view_manager.repeat_same_message(optional_params, message_id)

            serializer = serializers.VoiceMessageSerializers(repeated_voice_message, context={'request': request})

            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response('Something went wrong (Possible issue with the body of the content). ' +
                            'Error: ' + ex.__str__(),
                            status=status.HTTP_400_BAD_REQUEST)


class VoiceMessageOptionalsViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.

    Just for dummy data creation.

    For the actual creation of voice message please refer to /upload endpoint(s) or /voice-messages/with-options.
    """
    queryset = VoiceMessageOptionals.objects.all().order_by('id')
    serializer_class = serializers.VoiceMessageOptionalSerializers
    permission_classes = (permissions.IsAuthenticated,)


class PendingVoiceMessageList(ListAPIView):
    """
    Provides `list` action for pending messages, not readed messages.

    Account id should be given as the parameter to url.

    Device's unique identifier should be given as optional parameter the key 'uuid'.

    Ex. voice-messages/pendings/3?uuid=12345678-1234-5678-1234-567812345678

    **Note that**, the uuid in the example is the default uuid which is not accepted as a valid uuid for the server.
    If you want to use uuid specification of the server, you should start with assigning a valid uuid for the device.
    By using the exact uuid above, the query result you will see probably will not be valid.

    Controlling for the default uuid and not accepting it as the parameter value will be done soon. However,
    it will be stay as it is right now till the next step, for the purpose of testing.

    ** return ** the pending messages of given account.
    """
    serializer_class = serializers.PendingVoiceMessageSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        """
        Account id should be given as the parameter to url.

        Device's unique identifier should be given as optional parameter with the key 'uuid'.

        Ex. voice-messages/pendings/3?uuid=12345678-1234-5678-1234-567812345678

        **Note that**, the uuid in the example is the default uuid which is not accepted as a valid uuid for the server.
        If you want to use uuid specification of the server, you should start with assigning a valid uuid for the device
        . By using the exact uuid above, the query result you will see probably will not be valid.

        Controlling for the default uuid and not accepting it as the parameter value will be done soon. However,
        it will be stay as it is right now till the next step, for the purpose of testing.

        ** return ** the pending messages of given account.
        """
        account_id = self.kwargs['account_id']
        uuid = self.request.GET.get('uuid', None)
        if uuid is None:
            return VoiceMessages.objects.filter(to_account_id=account_id,
                                                is_read=False).order_by('sent_date')
        else:
            return VoiceMessages.objects.filter(to_account_id=account_id,
                                                is_read=False, to_device__unique_identifier=uuid).order_by('sent_date')


class InboxList(ListAPIView):
    """
    Provides `list` action for inbox messages, incoming messages of accounts.

    Account id should be given as the parameter to url.

    ** return ** the incoming messages of given account.
   """
    serializer_class = serializers.VoiceMessageSerializers
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        """
        Account id should be given as the parameter to url.

        ** return ** the incoming messages of given account.
        """
        account_id = self.kwargs['account_id']
        return VoiceMessages.objects.filter(to_device_id=6, to_account_id=account_id).order_by('-sent_date')[:20]


class OutboxList(ListAPIView):
    """
    Provides `list` action for outbox messages, outgoing messages of accounts.

    Account id should be given as the parameter to url.

    ** return ** the outgoing messages of given account.
    """
    serializer_class = serializers.VoiceMessageSerializers
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        """
        Account id should be given as the parameter to url.

        ** return ** the outgoing messages of given account.
        """
        account_id = self.kwargs['account_id']
        return VoiceMessages.objects.filter(from_account_id=account_id).order_by('-sent_date')[:20]
