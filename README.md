# README #

This README will explain how to connect and make use of VoiceIt-API.

### What is this repository for? ###

VoiceIt is a project that the essentials are based on the voice messages, between a hardware device and a phone. Currently, our app is only for iOS.

\* Version: There is no versioning yet. 

### How do I get set up? ###

* Summary of set up

   No need to setup anything. Just follow the link [http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/] to see the site. You will see a basic interface for the API. You can easily manage the requests with their URLS and methods following the site. 

* Usage of the Browsable API

   Browsable API is what you see when you enter the site. It does not give you the whole endpoints but those which are fundamental and especially have GET request. For example, you are not able to upload file from the Browsable API. Still, it gives you how it should look like so this tool is useful.

   **GET:** The URLs you are travelling and the JSON's you see in those pages are the response JSON's of the GET request for that URLs.

   **Other actions (Put, Post, Delete)** can be made for the same URLs by the specific request name. To see which request should be made, just traverse in the site. 

* Documentation

   Documentation is done with DRF Swagger and can be found in the endpoint /docs. Just go to through to the endpoint and see which method requires what and what are the response.

   However, it is lack of some features that you'd better to keep in mind. 

   1. It does not show the User POST method's body, properly. This is actually registration. POST to /users endpoint requires username, first_name, last_name, email and password in the body and it should be specified in the Header that Content-Type is application/json. 

   2. It also does not show /upload endpoints body, correctly. This is the file upload endpoint. The problem here is again with the body of the PUT method to /upload endpoint. Body should consist of from_account_id, to_account_id and file. Please note that file is really should be a file whereas _id's are actually integers but you might give it as a string also. 

* Notes

   API will work with Token Authentication. In order to be able to make request, whatever the type, be sure that you provide the right credentials. Credentials may vary according to the requests:

1. **Registration** During the registration phase, no credentials will be asked. You can simply register any user by making request. After the registration is complete, Token will be created. Clients should make requests with specific to the user. 
2. **Obtaining Token** To be able to make request, you need Token. You can obtain the Tokens for the users by requesting a POST to http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/api-token-auth/ with their username and password in the Body of type either form-data or JSON.
3. **Rest of the Requests** Rests of the requests, simply use the provided Token. Token should be placed in HTTP Header with key Authorization and value Token 3sd527b51d1badf034124523512d5ac91bac4dc (this is an example Token, will not work to make request). Assuming, providing Token correctly, you can now start making request with the appropriate Bodies (to check the Bodies refer to the website. Documentation will be provided soon as explained before).

### Contribution guidelines ###
* At First
   You need to setup the settings file as your own from the template that is avaliable within the project. Amazon Password to be able to use Amazon RDS and Subs. Keys for Microsot for both Speaker and Speech recognition are missing are those you have to provide. Please fill the blanks within the template and feel free to make use of it, in any manner you would like. And please ignore settings file to upload since I will be not accepting the commits with settings file in order not to disturb the template.

* Writing tests

   You all are welcome to write tests. Simply clone the project and make a test case for yourself. I will be pleased to see my faults with your test cases. I will be testing after I build the main infrastructure, but you are always welcome to do before.   

* Code review

   You can review the codes and make comment on me, whenever you like. However, as you are all ADMINS, you can be a part of my project and make comments on my current work based on my pull request. I, ideally, hold the pull request for 3 days. If there is no response, then I would simple merge it. Please fell free to approve or decline my pull requests other than commenting on them, but I sincerely ask you not to merge without my permission. I will merge both if 3 days are passed or all the admins gave me an approve. 

   \* Those do not cover the hotfixes. Since they are simple but forgotten fixes, I will merge them into the master. I will make them stay only for 3 hours, so if you want to review the fixes, you gotta be specifically watching for them - which is not necessary, in my opinion.

### Who do I talk to? ###

* This README is particularly designed for the ADMIN's of the VoiceIt.