# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-29 17:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_remove_account_test'),
        ('devices', '0002_auto_20161029_1306'),
    ]

    operations = [
        migrations.CreateModel(
            name='DevicesAndAccounts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('device_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='devices.Device')),
            ],
        ),
        migrations.AddField(
            model_name='device',
            name='accounts',
            field=models.ManyToManyField(through='devices.DevicesAndAccounts', to='accounts.Account'),
        ),
    ]
