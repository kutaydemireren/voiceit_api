from django.db import models
from accounts.models import Account

_DEFAULT_UUID_STR = '12345678-1234-5678-1234-567812345678'

__PHONE_DEVICE_ID = 6


def phone_device_id():
    return __PHONE_DEVICE_ID


class Device(models.Model):
    """
        Base class for the devices
    """
    unique_identifier = models.UUIDField(null=False, default=_DEFAULT_UUID_STR)
    name = models.CharField(max_length=30, blank=True, null=True)

    accounts = models.ManyToManyField(Account, through='DevicesAndAccounts')

    def __str__(self):
        return 'Device ' + self.name


class DevicesAndAccounts(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    used_count = models.IntegerField(null=True, default=0)

    def __str__(self):
        return 'Device ' + self.device.name + ' - Account ' + self.account.username
