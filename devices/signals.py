from django.db.models.signals import post_save
from django.dispatch import receiver

from devices.models import DevicesAndAccounts, phone_device_id
from voice_messages.models import VoiceMessages


@receiver(post_save, sender=VoiceMessages)
def create_account_to_device_relation(sender, instance, created, **kwargs):
    if created:
        account = instance.from_account
        device = instance.to_device
        if device.id != phone_device_id():
            queryset = DevicesAndAccounts.objects.filter(account=account,
                                                         device=device)
            if queryset.exists():
                account_to_device_inst = queryset.first()
                account_to_device_inst.used_count += 1
                account_to_device_inst.save()
            else:
                DevicesAndAccounts.objects.create(device=device, account=account, used_count=1)
