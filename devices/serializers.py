from rest_framework import serializers
from devices.models import Device, DevicesAndAccounts
from accounts.serializers import AccountSerializer
from accounts.models import AccountToAccount
from rest_framework.validators import UniqueTogetherValidator
from uuid import UUID

_DEFAULT_UUID = UUID('12345678-1234-5678-1234-567812345678')
_SEND_COUNT_FIELD = 'send_message_count'


class DeviceSerializers(serializers.ModelSerializer):
    accounts = AccountSerializer(many=True, read_only=True)
    unique_identifier = serializers.UUIDField(write_only=True)
    uuid = serializers.SerializerMethodField()

    class Meta:
        model = Device
        fields = ('id', 'unique_identifier', 'uuid', 'name', 'accounts')

    def get_uuid(self, obj):
        if _DEFAULT_UUID == obj.unique_identifier:
            return 'Invalid UUID. Please update UUID to use.'
        else:
            return obj.unique_identifier


class DeviceAndAccountSerializers(serializers.ModelSerializer):
    account_id = serializers.IntegerField(write_only=True)
    device_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = DevicesAndAccounts
        fields = ('id', 'account_id', 'device_id')

        validators = [
            UniqueTogetherValidator(
                queryset=DevicesAndAccounts.objects.all(),
                fields=('account_id', 'device_id')
            )
        ]


class DevicesBoundToAccountSerializer(serializers.ModelSerializer):
    accounts = serializers.SerializerMethodField()
    uuid = serializers.SerializerMethodField()

    class Meta:
        model = Device
        fields = ('id', 'uuid', 'name', 'accounts', )

    def get_uuid(self, obj):
        if _DEFAULT_UUID == obj.unique_identifier:
            return 'Invalid UUID. Please update UUID to use.'
        else:
            return obj.unique_identifier

    def get_accounts(self, obj):
        from_account_id = self.context.get('account_id')
        accounts_in_device = obj.accounts.all()

        accounts_with_counts_dict = dict()
        not_messaged_accounts = []
        for account in accounts_in_device:
            query = AccountToAccount.objects.\
                filter(from_account_id=from_account_id, to_account_id=account).values(_SEND_COUNT_FIELD)

            if query.count() > 0:
                accounts_with_counts_dict[account] = query
            else:
                not_messaged_accounts += [account]

        accounts = []
        for key, value in sorted(accounts_with_counts_dict.items(), key=lambda kv: -kv[1].first()[_SEND_COUNT_FIELD]):
            accounts += [key]

        accounts.extend(not_messaged_accounts)

        serializer = AccountSerializer(accounts, many=True)
        return serializer.data
