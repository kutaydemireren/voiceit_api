from django.conf.urls import url
from devices import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'devices', views.DeviceViewSet)

urlpatterns = [
    url(r'^account_and_device/$', views.AccountAndDeviceView.as_view()),
    url(r'^devices/bound_to/(?P<account_id>\d+)$', views.DevicesBoundToAccountView.as_view()),
    url(r'^devices/(?P<device_id>\d+)/has-message$', views.DeviceHasMessageView.as_view()),
    url(r'^devices/(?P<device_id>\d+)/change-name$', views.UpdateDeviceName.as_view()),
]

routeList = (
    (r'devices', views.DeviceViewSet),
)
