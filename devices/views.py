from rest_framework import permissions, viewsets
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.views import APIView
from rest_framework.response import Response

from devices.models import Device, DevicesAndAccounts
from devices.serializers import DeviceSerializers, DevicesBoundToAccountSerializer

from voice_messages.models import VoiceMessages


class DeviceViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
    """
    queryset = Device.objects.all().order_by('id')
    serializer_class = DeviceSerializers
    permission_classes = (permissions.IsAuthenticated,)


class AccountAndDeviceView(APIView):
    """
    Provides 'create' and 'destroy' actions.

    Body requires an account id with the key name 'account_id' and device uuid with the key name 'device_uuid'.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            account_id = request.data['account_id']
            device_uuid = request.data['device_uuid']

            device = Device.objects.get(unique_identifier=device_uuid)

            if device.id > 0:
                new_relation = DevicesAndAccounts.objects.create(account_id=account_id,
                                                                 device_id=device.id)

                if new_relation.id > 0:
                    return Response('success', status=HTTP_201_CREATED)
                else:
                    return Response('Something went wrong. Possible issue with the body.', status=HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response('Something went wrong.'
                            'Error: ' + ex.__str__(), status=HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        try:
            account_id = request.data['account_id']
            device_id = request.data['device_id']

            del_total_count, del_obs_count = DevicesAndAccounts.objects.filter(account_id=account_id,
                                                                               device_id=device_id) \
                .delete()

            if del_total_count > 0:
                return Response('success', status=HTTP_200_OK)
            else:
                return Response('Something went wrong (Possible issue with the body). ', status=HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response('Something went wrong.'
                            'Error: ' + ex.__str__(), status=HTTP_500_INTERNAL_SERVER_ERROR)


class DevicesBoundToAccountView(APIView):
    """
    Provides 'list' action for devices that are bound to given account
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, account_id):
        device_ids_bound_to_account = DevicesAndAccounts.objects.filter(account_id=account_id) \
            .order_by('-used_count').values('device_id')

        devices = []
        for d_id in device_ids_bound_to_account:
            device = Device.objects.get(pk=d_id['device_id'])
            devices.append(device)

        serializer = DevicesBoundToAccountSerializer(devices, many=True, context={'account_id': account_id})
        return Response(serializer.data)


class DeviceHasMessageView(APIView):
    """
    Returns whether device has a message in it or not. Designed for board.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, device_id):
        try:
            device = Device.objects.get(pk=device_id)

            for account in device.accounts.all():
                queryset = VoiceMessages.objects.filter(to_account=account, to_device=device)
                if queryset.count() > 0:
                    return Response(True, HTTP_200_OK)
            return Response(False, HTTP_200_OK)
        except Exception as ex:
            return Response('Something went wrong. Error: ' + ex.__str__())


class UpdateDeviceName(APIView):
    """
    Updates the devices' name
    """
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request, device_id):
        try:
            device = Device.objects.get(pk=device_id)
            update_name = request.data['name']

            device.name = update_name
            device.save()
            return Response('success', status=HTTP_200_OK)
        except Exception as ex:
            return Response('Something went wrong. Error: ' + ex.__str__())
