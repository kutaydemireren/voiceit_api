import requests
from voiceit.settings import OCM_APIM_SUBSCRIPTION_KEY_SPEECH_RECOGNITION as SUBSCRIPTION_KEY
import uuid

from speech_recognition_client.managers import get_token

import io

#urllib3.disable_warnings()
#logging.captureWarnings(True)


parameters = [
    "scenarios=ulm",
    "appID=D4D52672-91D7-4C74-8AD8-42B1D98141A5",
    "locale=en-us",
    "device.os=iPhone OS",
    "version=3.0",
    "format=json",
    "requestid=" + uuid.uuid1().__str__(),
    "instanceid=565D69FF-E928-4B7E-87DA-9A750B96D9E3",
    "result.profanitymarkup=1"
]

#scenario can be also websearch/ result.
# profanitymarkup  -> Scan the result text for words included in an offensive word list.
# If found, the word will be delimited by bad word tag.


def create_url():
    url_base = "https://speech.platform.bing.com/recognize?"

    count = len(parameters)
    i = 0
    for param in parameters:
        url_base += param
        if (i+1) != count:
            url_base += '&'
        i += 1
    return url_base


def create_request(file):
    token = get_token()

    headers = {
        'Content-Type': 'audio/wav',
        'codec': 'audio/pcm',
        'samplerate': '16000',
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
        'Authorization': 'Bearer ' + token
    }

    url = create_url()

    try:
        response = requests.post(url, data=file, headers=headers)
        return response
    except Exception:
        print("error")


def speech_to_text_get_lex(response):
    response_json=response.json()
    resp=response_json

    results=resp['results']

    result_array=results[0]
    lex=result_array['lexical']

    return lex

def speech_to_text_get_status(response):
    response_json = response.json()
    resp = response_json

    header = resp['header']
    stat = header['status']

    return stat