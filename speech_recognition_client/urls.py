from django.conf.urls import url
from speech_recognition_client import views

urlpatterns = [
    url(r'^speech-to-text$',  views.CreateRequestView.as_view()),
]