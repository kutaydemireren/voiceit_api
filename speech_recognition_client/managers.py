import requests
from voiceit.settings import OCM_APIM_SUBSCRIPTION_KEY_SPEECH_RECOGNITION as SUBSCRIPTION_KEY
import threading

__is_token_expired = True
__refresh_token_duration = 600  # 10 min in seconds
__token = ''


def __set_token_expired(token_status):
    global __is_token_expired
    __is_token_expired = token_status


def __get_issue_token():
    headers = {
        # Request headers
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    url = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken"

    try:
        response = requests.post(url, headers=headers)

        if __is_token_expired:
            threading.Timer(__refresh_token_duration, __set_token_expired, [True]).start()
            __set_token_expired(False)

        global __token
        __token = response.text
        return response.text
    except Exception:
        print("error")


def get_token():
    if __is_token_expired:
        token = __get_issue_token()
        return token
    return __token
