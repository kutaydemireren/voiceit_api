from __future__ import unicode_literals

from django.apps import AppConfig


class SpeechRecognitionClientConfig(AppConfig):
    name = 'speech_recognition_client'
