from rest_framework import permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response

from speech_recognition_client import speech



# Create your views here.
class CreateRequestView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        file = request.data['file']
        response = speech.create_request(file=file)
        lex = speech.speech_to_text_get_lex(response)
        stat=speech.speech_to_text_get_status(response)

        if response.ok:
            if stat == "success":
                return Response(lex, status=status.HTTP_200_OK)

            else:
                return Response(stat, status=status.HTTP_204_NO_CONTENT)

        else:
            return Response(response.json()['error']['message'], status=status.HTTP_400_BAD_REQUEST)





