from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework import permissions, viewsets, renderers
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.models import Account
from accounts.serializers import AccountSerializer, UserSerializer
from django.contrib.auth.models import User
from accounts import view_manager


class AllowCreateOrRequireAuthenticate(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST" and not request.user.is_authenticated():
            return True
        elif request.method != "POST" and request.user.is_authenticated():
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated():
            return False
        elif request.method in permissions.SAFE_METHODS:
            return True

        return obj.username == request.user.username


class NoAuthenticate(permissions.BasePermission):
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        return True


class UserViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
    """
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer
    permission_classes = (AllowCreateOrRequireAuthenticate,)


class AccountViewSet(viewsets.ModelViewSet):
    """
    Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
    """
    queryset = Account.objects.all().order_by('id')
    serializer_class = AccountSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ProfilePhotoUploadView(APIView):
    """
    Uploads the profile photo of the user.

    Request requires only the key 'file' and a file value for it, in the body.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request):
        is_success, response = view_manager.upload_profile_photo(request)

        if is_success:
            account = response
            serializer = AccountSerializer(account, context={'request': request})
            return Response(serializer.data, status=HTTP_201_CREATED)
        else:
            error_message = response
            return Response(error_message, status=HTTP_400_BAD_REQUEST)


class ObtainAuthToken(APIView):
    """
    Provides the token with account detail
    """
    permission_classes = (NoAuthenticate,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        account_serializer = AccountSerializer(user.account, context={'request': request})
        return Response({'account': account_serializer.data,
                         'token': token.key})
