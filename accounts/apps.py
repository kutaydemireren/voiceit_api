from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AccountConfig(AppConfig):
    name = 'accounts'
    verbose_name = _('accounts')

    def ready(self):
        import accounts.signals
