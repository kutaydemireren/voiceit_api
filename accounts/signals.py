from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from accounts.models import Account, AccountToAccount
from voice_messages.models import VoiceMessages


@receiver(post_save, sender=User)
def create_user_account(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_account(sender, instance, **kwargs):
    instance.account.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=VoiceMessages)
def create_account_to_account_relation(sender, instance, created, **kwargs):
    if created:
        queryset = AccountToAccount.objects.filter(from_account=instance.from_account,
                                                   to_account=instance.to_account)

        if queryset.exists():
            account_to_account_inst = queryset.first()
            account_to_account_inst.send_message_count += 1
            account_to_account_inst.save()
        else:
            AccountToAccount.objects.create(from_account=instance.from_account, to_account=instance.to_account)
