from voiceit.AWS import AWSClient


def upload_profile_photo(request):
    """
    Upload the file and update the DB for the user
    """
    file = request.FILES['file']
    user = request.user

    aws_client = AWSClient()
    is_success, filename = aws_client.put_profile_photo(profile_photo_bin=file, account=user.account)

    if is_success:
        is_success, response = __set_profile_photo(account=user.account,
                                                   bucket_name=aws_client.bucket_name,
                                                   folder=aws_client.get_profile_photo_folder_name(),
                                                   pp_name=filename)
        return is_success, response


def __set_profile_photo(account, bucket_name, folder, pp_name):
    """
    Returns True with updated account or False with error message
    """
    try:
        account.bucket_name = bucket_name
        account.bucket_key = folder
        account.profile_photo_name = pp_name
        account.save()

        return True, account
    except Exception as ex:
        return False, 'Error at set profile photo. Error: ' + ex.__str__()
