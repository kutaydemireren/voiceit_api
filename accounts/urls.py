from django.conf.urls import url
from accounts import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'accounts', views.AccountViewSet)
router.register(r'users', views.UserViewSet)

urlpatterns = [
    url(r'^accounts/upload-profile-photo', views.ProfilePhotoUploadView.as_view()),
    url(r'^api-token-auth$', views.ObtainAuthToken.as_view()),
]

routeList = (
    (r'accounts', views.AccountViewSet),
    (r'users', views.UserViewSet),
)
