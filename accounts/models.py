from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    """
        Base class for the user accounts.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bucket_name = models.CharField(max_length=30, null=True)
    bucket_key = models.CharField(max_length=30, null=True)
    profile_photo_name = models.CharField(max_length=30, null=True)

    @property
    def username(self):
        return self.user.username

    @property
    def email(self):
        return self.user.email

    @property
    def profile_photo_url(self):
        return "http://s3.eu-central-1.amazonaws.com/" + self.bucket_name + "/" + \
               self.bucket_key + "/" + self.profile_photo_name

    def __str__(self):
        return self.username + "'s Account"


class AccountToAccount(models.Model):
    """
        Base class for the user accounts.
    """
    from_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account_to_account_from_account')
    to_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account_to_account_to_account')
    send_message_count = models.IntegerField(null=True, default=1)

    class Meta:
        unique_together = ('from_account', 'to_account',)

    def __str__(self):
        return 'From ' + self.from_account.__str__() + ' To ' + self.to_account.__str__()
