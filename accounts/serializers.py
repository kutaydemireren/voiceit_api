from django.contrib.auth.models import User
from rest_framework import serializers
from accounts.models import Account, AccountToAccount


class AccountSerializer(serializers.ModelSerializer):
    pp = serializers.SerializerMethodField()

    class Meta:
        model = Account
        fields = ('id', 'username', 'email', 'pp')

    def get_pp(self, obj):
        is_bucket_key_none = obj.bucket_key is None
        is_bucket_name_none = obj.bucket_name is None
        is_profile_photo_name_none = obj.profile_photo_name is None

        is_profile_photo_url_none = is_bucket_key_none and is_bucket_name_none and is_profile_photo_name_none

        if is_profile_photo_url_none:
            return ''
        else:
            return obj.profile_photo_url


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    account = AccountSerializer(many=False, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'first_name', 'last_name', 'email', 'password', 'account')

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class AccountToAccountSerializer(serializers.ModelSerializer):
    accounts = serializers.SerializerMethodField()

    class Meta:
        model = AccountToAccount
        fields = ('accounts',)

    def get_accounts(self, obj):
        from_account_id = self.context.get('from_account_id')
        to_account_ids = AccountToAccount.objects.filter(from_account_id=from_account_id).\
            order_by('-send_message_count').values('to_account_id')

        accounts = []
        for acc_id in to_account_ids:
            account = Account.objects.get(pk=acc_id['to_account_id'])

            if account in obj.accounts.all():
                accounts.append(account)

        serializer = AccountSerializer(accounts, many=True)
        return serializer.data
