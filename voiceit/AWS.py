import boto3
from boto3 import session
from voiceit import settings, sockets
from voiceit.common_methods import random_ascii_string
from voice_messages.models import VoiceMessages, VoiceMessageOptionals
from accounts.models import Account
from devices.models import Device
from datetime import datetime

_FILENAME_LENGTH = 15


class AWSClient:
    # Supported file types (exts)
    audio_exts = ["wav"]
    image_exts = ["jpg", "png"]

    # dirnames at AWS S3
    __voice_record = "voice-records"
    __voice_message = "voice-messages"
    __profile_photo = "profile-photos"

    def __init__(self):
        self.env = {
            "aws_access_key_id": settings.AWS_ACCESS_KEY_ID,
            "aws_secret_access_key": settings.AWS_SECRET_ACCESS_KEY,
            "region_name": 'eu-central-1'
        }

        self.bucket_name = "voiceitbucket"
        self.session = session.Session(region_name='eu-central-1')
        self.s3_client = self.session.client('s3', config=boto3.session.Config(signature_version='s3v4'))
        self.bucket_url = "https://s3.eu-central-1.amazonaws.com/"

    def __true(self, str_data):
        """
        Str should be string type
        """
        return str_data == 'True'

    def __is_audio_ext(self, ext):
        return ext.lower() in self.audio_exts

    def __is_image_ext(self, ext):
        return ext.lower() in self.image_exts

    def put(self, file_bin_data, req_data, folder):
        ext = req_data['file'].name.split('.')[-1]
        filename = self.__create_random_filename(ext)

        if self.__is_audio_ext(ext):
            is_success, response = False, ''
            if folder == self.__voice_record:
                self.__create_voice_record(req_data, folder, filename)
            elif folder == self.__voice_message:
                is_success, response = self.__create_voice_messages(req_data, folder, filename)
            else:
                return False, "Response cannot be constructed. Please be sure to give right folder name."

            if is_success:
                self.s3_client.put_object(Body=file_bin_data, Bucket=self.bucket_name, Key=folder + "/" + filename)

                return True, "Successfully created at URL " + self.bucket_url + self.bucket_name + "/" \
                       + self.__voice_message + "/" + filename
            else:
                return False, response
        else:
            return False, "Invalid file type"

    def put_with_optionals(self, file_bin_data, req_data, opt_params):
        ext = req_data['file'].name.split('.')[-1]
        filename = self.__create_random_filename(ext)

        if self.__is_audio_ext(ext):
            is_success_msg, response_msgs = self.__create_voice_messages(req_data, self.__voice_message, filename)
            is_success_opts, response_opts = self.__set_optionals_for_voice_messages(opt_params, response_msgs)

            is_success = is_success_msg and is_success_opts

            if is_success:
                self.s3_client.put_object(Body=file_bin_data,
                                          Bucket=self.bucket_name,
                                          Key=self.__voice_message + "/" + filename)

                return True, "Successfully created at URL " + self.bucket_url + self.bucket_name + "/" \
                       + self.__voice_message + "/" + filename
            else:
                response = 'From create message: ' + response_msgs.__str__() +\
                           ' and from create optionals ' + response_opts.__str__()
                return False, response
        else:
            return False, 'Invalid file type'

    def put_profile_photo(self, profile_photo_bin, account):
        ext = profile_photo_bin.name.split('.')[-1]
        filename = self.__create_random_filename(ext)

        if self.__is_image_ext(ext):
            self.s3_client.put_object(Body=profile_photo_bin,
                                      Bucket=self.bucket_name,
                                      Key=self.__profile_photo + "/" + filename)

            return True, filename
        else:
            return False, 'Invalid file type'

    def delete(self, folder, filename):
        self.s3_client.delete_object(
            Bucket=self.bucket_name,
            Key=folder + '/' + filename
        )

    def __create_voice_messages(self, data, folder, filename):
        try:
            t_acc_ids = data['to_account_ids']
            t_acc_ids = [int(i) for i in t_acc_ids.strip('[]').split(',')]

            if type(t_acc_ids) is not list:
                return False, "Wrong type of to_account_ids. " \
                              "to_account_ids must be type of list -> [3, 4, 5, 6] "

            f_acc = Account.objects.get(pk=data['from_account_id'])

            if 'to_device_id' not in data.keys():
                t_dev = Device.objects.get(pk=6)
                f_dev_id = data['from_device_id']
                f_dev = Device.objects.get(pk=f_dev_id)
            else:
                t_dev = Device.objects.get(pk=data['to_device_id'])
                f_dev = Device.objects.get(pk=6)

            voice_messages = []
            for t_acc_id in t_acc_ids:
                t_acc = Account.objects.get(pk=t_acc_id)
                voice_message = self.__create_voice_message(folder, filename, f_acc, t_acc, f_dev,  t_dev)
                voice_messages.append(voice_message)
            return True, voice_messages
        except:
            return False, 'Something went wrong. Please check the body.'

    def __create_voice_message(self, folder, filename, f_acc, t_acc, f_dev, t_dev):
        sent_date = datetime.now()

        voice_message = VoiceMessages.objects.create(from_account=f_acc,
                                                     to_account=t_acc,
                                                     from_device=f_dev,
                                                     to_device=t_dev,
                                                     is_sent=True,
                                                     sent_date=sent_date,
                                                     is_read=False,
                                                     read_date=None,
                                                     bucket_name=self.bucket_name,
                                                     bucket_key=folder,
                                                     filename=filename)

        return voice_message

    def __create_voice_record(self, data, folder, filename):
        return NotImplementedError

    def __create_random_filename(self, ext):
        filename = random_ascii_string(_FILENAME_LENGTH)
        filename = filename + '.' + ext

        return filename

    def __set_optionals_for_voice_messages(self, opt_params, voice_messages):
        will_notify = False
        try:
            set_date = opt_params['set_date']
            if set_date is None:
                is_set_time = False
                expiration_date = None  # expiration date must be None no matter what, if set date is None
            else:
                is_set_time = True

                expiration_date = opt_params['expiration_date']
                expiration_date_split = expiration_date.split('_')
                expiration_date = expiration_date_split[0] + ' ' + expiration_date_split[1]

                set_date_split = set_date.split('_')
                set_date = set_date_split[0] + ' ' + set_date_split[1]

                will_notify = True

            repeat_time = opt_params['repeat_time']
            if repeat_time is None:
                will_repeat = False
                will_repeat_by_time = False
            else:
                will_repeat = True
                will_repeat_by_time = True

                repeat_time_split = repeat_time.split('_')
                repeat_time = repeat_time_split[0] + ' ' + repeat_time_split[1]

                will_notify = True

            constructed_params = {
                'is_set_time': is_set_time,
                'set_date': set_date,
                'expiration_date': expiration_date,
                'will_repeat': will_repeat,
                'will_repeat_by_time': will_repeat_by_time,
                'repeat_time': repeat_time,
            }

            # if will_notify:
            #     self.send_notifications(constructed_params, voice_messages)

            for voice_message in voice_messages:
                self.__set_optionals(constructed_params, voice_message)

            return True, 'Success'
        except Exception as ex:
            return False, 'Something went wrong. Error: ' + ex.__str__()

    def __set_optionals(self, opt_params, voice_message):
        voice_message_optionals = VoiceMessageOptionals.objects.get(voice_message=voice_message)

        voice_message_optionals.is_set_time = opt_params['is_set_time']
        voice_message_optionals.set_date = opt_params['set_date']
        voice_message_optionals.expiration_date = opt_params['expiration_date']
        voice_message_optionals.will_repeat = opt_params['will_repeat']
        voice_message_optionals.will_repeat_by_time = opt_params['will_repeat_by_time']
        voice_message_optionals.repeat_time = opt_params['repeat_time']

        voice_message_optionals.save()

    def __set_profile_photo(self, account, pp_name):
        account.bucket_name = self.bucket_name
        account.bucket_key = self.__profile_photo
        account.profile_photo_name = pp_name
        account.save()

        return account

    def get_bucket_name(self):
        return self.bucket_name

    def get_voice_record_folder_name(self):
        return self.__voice_record

    def get_voice_message_folder_name(self):
        return self.__voice_message

    def get_profile_photo_folder_name(self):
        return self.__profile_photo

    def __send_set_date_notification(self, set_date, expiration_date, voice_messages):
        for voice_message in voice_messages:
            message = 'set date: ' + set_date.__str__() + ', expiration date: ' + expiration_date.__str__() + \
                      ', voice message id ' + str(voice_message.id)
            sockets.notify_port(message=message)

    def __send_repeat_time_notification(self, repeat_time, voice_messages):
        pass


    def send_notifications(self, constructed_params, voice_messages):
        is_set_time = constructed_params['is_set_time']
        if is_set_time:
            set_date = constructed_params['set_date']
            expiration_date = constructed_params['expiration_date']
            self.__send_set_date_notification(set_date=set_date,
                                              expiration_date=expiration_date,
                                              voice_messages=voice_messages)

        will_repeat = constructed_params['will_repeat']
        if will_repeat:
            will_repeat_by_time = constructed_params['will_repeat_by_time']
            if will_repeat_by_time:
                repeat_time = constructed_params['repeat_time']
                self.__send_repeat_time_notification(repeat_time=repeat_time,
                                                     voice_messages=voice_messages)
