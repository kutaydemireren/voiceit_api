"""voiceit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter
from accounts import urls as accountUrls
from devices import urls as deviceUrls
from voice_messages import urls as voice_messageUrls

from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view


schema_view = get_swagger_view(title='VoiceIt API')

routeLists = [
    accountUrls.routeList,
    deviceUrls.routeList,
    voice_messageUrls.routeList
]


router = DefaultRouter()
for routeList in routeLists:
    for route in routeList:
        if len(route) > 2:
            router.register(route[0], route[1], route[2])
        else:
            router.register(route[0], route[1])

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^', include('voice_messages.urls')),
    url(r'^', include('speaker_recognition_client.urls')),
    url(r'^', include('speech_recognition_client.urls')),
    url(r'^', include('devices.urls')),
    url(r'^', include('accounts.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^docs/$', schema_view),
]
