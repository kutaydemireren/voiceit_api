import socket

__TCP_IP = socket.gethostbyaddr("52.57.173.246")[0]
__TCP_PORT = 5000
__BUFFER_SIZE = 1024


def notify_port(message):
    if type(message) == str:
        message = message.encode()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((__TCP_IP, __TCP_PORT))
    s.send(message)
    data = s.recv(__BUFFER_SIZE)
    s.close()

    print("received data:", data)
