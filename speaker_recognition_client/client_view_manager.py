from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_500_INTERNAL_SERVER_ERROR, \
    HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST

from accounts.serializers import AccountSerializer
from speaker_recognition_client import client_base, model_helper
from speaker_recognition_client import errors
from speaker_recognition_client.responses import EnrollmentResponse

# external types
_OPERATION_LOCATION_HEADER = 'Operation-Location'
_STATUS_FIELD_NAME = 'status'
_STATUS_SUCCESS_RETURN = 'succeeded'
_STATUS_FAILURE_RETURN = 'failed'
_STATUS_NOT_STARTED_RETURN = 'notstarted'
_STATUS_RUNNING_RETURN = 'running'
_OPERATION_PROC_RES_FIELD_NAME = 'processingResult'
_OPERATION_CONFIDENCE_FIELD_NAME = 'confidence'
_IDENTIFIED_ID_FIELD_NAME = 'identifiedProfileId'
_ERROR_FIELD_NAME = 'error'
_FAILURE_MESSAGE_FIELD_NAME = 'message'
_SPEAKER_NOT_FOUND_IDENTIFIER_ID = '00000000-0000-0000-0000-000000000000'

# internal types
_RESPONSE_FIELD_NAME = 'response'
_SUCCESS_RETURN = 'success'
_FAILURE_RETURN = 'failure'
_REPEAT_TIME = 10
_SLEEP_TIME = 10
_FILE_FIELD_NAME = 'file'


def get_operation_proc_result(operation_status_url):
    """
    :param operation_status_url: operation url to be checked
    :return: processingResult value of response as JSON
    """
    while True:
        response = client_base.get_operation_proc_result(operation_status_url)

        if response is None:
            error_extra_inf = 'error occured at: speaker_recognition_client.client_view_manager.' \
                              'get_operation_proc_result > response is none type'
            return False, _FAILURE_RETURN, errors.none_type_object(error_extra_inf)

        if response.ok:
            response = response.json()
            status = response[_STATUS_FIELD_NAME]

            if status == _STATUS_SUCCESS_RETURN:
                operation_response = {_RESPONSE_FIELD_NAME: response[_OPERATION_PROC_RES_FIELD_NAME],
                                      _STATUS_FIELD_NAME: HTTP_200_OK}
                return True, _STATUS_SUCCESS_RETURN, operation_response
            elif status == _STATUS_FAILURE_RETURN:
                operation_response = {_RESPONSE_FIELD_NAME: response[_FAILURE_MESSAGE_FIELD_NAME],
                                      _STATUS_FIELD_NAME: HTTP_200_OK}
                return False, _STATUS_FAILURE_RETURN, operation_response
        else:
            return False, _FAILURE_RETURN, errors.microsoft_error(response)


def enroll_profile(account_id, file):
    """
    :param account_id: to be assigned to file
    :param file: file to given account
    :return: response of the enrollment: how much time left, how much time is done, enrolled?
    """
    identification_profile_id = model_helper.get_identification_profile_id(account_id)
    response = client_base.enroll_profile(identification_profile_id=identification_profile_id, file=file)

    if response is None:
        error_extra_inf = 'error occured at: speaker.recognition_client.client_view_manager.' \
                          'enroll_profile > response is none type'
        return errors.none_type_object(error_extra_inf)

    if response.ok:
        operation_status_url = response.headers[_OPERATION_LOCATION_HEADER]
        update_response = model_helper.update_enrollment_status_url(account_id, operation_status_url)

        if update_response == _SUCCESS_RETURN:
            is_operation_success, status_return, operation_response = get_operation_proc_result(operation_status_url)

            if is_operation_success:
                enrollment_response = EnrollmentResponse(operation_response[_RESPONSE_FIELD_NAME]).data()

                server_response = {'response': enrollment_response,
                                   'status': HTTP_201_CREATED}
                return server_response
            else:
                server_response = {'response': operation_response[_RESPONSE_FIELD_NAME],
                                   'status': operation_response[_STATUS_FIELD_NAME]}
                return server_response

    else:
        return errors.microsoft_error(response)


def __create_server_response(success, account, confidence, status):
    server_response = {'success': success,
                       'account': account,
                       'confidence': confidence
                       }

    return Response(server_response, status=status)


def identification(request):
    file = request.data[_FILE_FIELD_NAME]

    count = model_helper.get_objects_count()
    times = count / 10 + 1

    success = False
    status = HTTP_404_NOT_FOUND
    account = ''
    confidence = ''
    for i in range(0, times):
        begin = i * 10
        identification_profile_ids = model_helper.get_identification_profile_ids_between(begin, begin + 10)
        comma_separated_identification_profile_ids = separate_list_elts_by_comma(identification_profile_ids)

        response = client_base.identification(comma_separated_identification_profile_ids, file=file)

        if response is None:
            error_extra_inf = 'error occured at: speaker.recognition_client.client_view_manager.' \
                              'identification > response is none type'
            error = errors.none_type_object(error_extra_inf)
            return __create_error_response(error)

        if response.ok:
            operation_status_url = response.headers[_OPERATION_LOCATION_HEADER]
            is_operation_success, status_return, operation_response = get_operation_proc_result(operation_status_url)

            if is_operation_success:
                operation_proc_response = operation_response[_RESPONSE_FIELD_NAME]
                identified_speaker_uuid = operation_proc_response[_IDENTIFIED_ID_FIELD_NAME]
                confidence = operation_proc_response[_OPERATION_CONFIDENCE_FIELD_NAME]

                if not (identified_speaker_uuid == _SPEAKER_NOT_FOUND_IDENTIFIER_ID):  # found
                    success = 'True'
                    status = HTTP_200_OK

                    account_main = model_helper.get_account_from_identification_profile_id(identified_speaker_uuid)
                    serializer = AccountSerializer(account_main, context={'request': request})
                    account = serializer.data

                    return __create_server_response(success, account, confidence, status)

            else:
                if status_return == _STATUS_FAILURE_RETURN:
                    return Response(operation_response[_RESPONSE_FIELD_NAME],
                                    status=operation_response[_STATUS_FIELD_NAME])
                else:
                    error = operation_response
                    return __create_error_response(error)
        elif response.status_code == 400:
            error_message = response.json()[_ERROR_FIELD_NAME][_FAILURE_MESSAGE_FIELD_NAME]
            extra_error_information = 'You can check your voice mail. Please read the specifications.'
            error_status = HTTP_400_BAD_REQUEST

            error = errors.custom_error(error_message=error_message,
                                        error_status=error_status,
                                        extra_error_inf=extra_error_information)
            return __create_error_response(error)
        else:
            error_message = 'Unexpected behaviour'
            error_status = HTTP_500_INTERNAL_SERVER_ERROR
            extra_error_inf = 'error occured at: speaker.recognition_client.client_view_manager.' + \
                              'identification > response is not ok'
            error = errors.custom_error(error_message=error_message,
                                        error_status=error_status,
                                        extra_error_inf=extra_error_inf)

            return __create_error_response(error)

    return __create_server_response(success, account, confidence, status)


def identify_speaker(file):
    """
    **Deprecated**
    """
    message_trace = []
    count = model_helper.get_objects_count()
    times = count / 10 + 1
    for i in range(0, times):
        begin = i * 10 + 1
        identification_profile_ids = model_helper.get_identification_profile_ids_between(begin, begin + 10)
        comma_seperated_identification_profile_ids = separate_list_elts_by_comma(identification_profile_ids)

        response = client_base.identification(comma_seperated_identification_profile_ids, file=file)

        is_speaker_found, message_or_proc = check_speaker_found(response)

        if is_speaker_found:
            speaker_id = message_or_proc[0]
            confidence = message_or_proc[1]
            return _SUCCESS_RETURN, [speaker_id, confidence]
        else:
            message = message_or_proc
            message_trace.append(message)

    return _FAILURE_RETURN, message_trace


def separate_list_elts_by_comma(lst):
    return_str = ''
    count = len(lst)
    i = 0
    for elt in lst:
        if not isinstance(elt, str):
            elt = str(elt)
        return_str += elt
        if (i + 1) != count:
            return_str += ','
        i += 1

    return return_str


def check_speaker_found(response):
    operation_status_url = response.headers[_OPERATION_LOCATION_HEADER]
    operation_status_response = client_base.get_operation_status(operation_status_url)
    operation_status_response_json = operation_status_response.json()
    operation_status = operation_status_response_json[_STATUS_FIELD_NAME]

    for i in range(0, _REPEAT_TIME):
        if operation_status == _STATUS_SUCCESS_RETURN:
            # Acc. to Microsoft Speaker Recognition API, there are four status response:
            # 'notstarted', 'running', 'failed', 'succeeded'
            # here is the 'succeeded' part
            operation_proc_response = operation_status_response_json[_OPERATION_PROC_RES_FIELD_NAME]
            identified_id = operation_proc_response[_IDENTIFIED_ID_FIELD_NAME]
            confidence = operation_proc_response[_OPERATION_CONFIDENCE_FIELD_NAME]
            if identified_id == _SPEAKER_NOT_FOUND_IDENTIFIER_ID:
                return False, 'Speaker could not identified'
            else:  # found
                return True, [identified_id, confidence]

        elif operation_status == _STATUS_FAILURE_RETURN:
            # Acc. to Microsoft Speaker Recognition API, there are four status response:
            # 'notstarted', 'running', 'failed', 'succeeded'
            # here is the 'failed' part
            return False, operation_status_response_json[_FAILURE_MESSAGE_FIELD_NAME]
        else:
            # Acc. to Microsoft Speaker Recognition API, there are four status response:
            # 'notstarted', 'running', 'failed', 'succeeded'
            # If not neither above, it is either 'notstarted' or 'running'. So, should keep waiting.
            operation_status_response = client_base.get_operation_status(operation_status_url)
            operation_status_response_json = operation_status_response.json()
            operation_status = operation_status_response_json[_STATUS_FIELD_NAME]

    return False, operation_status


def __create_error_response(error):
    error_response = {'error_message': error['error_message'],
                      'extra_error_inf': error['extra_error_inf']
                      }
    error_status = error['error_status']
    return Response(error_response, status=error_status)
