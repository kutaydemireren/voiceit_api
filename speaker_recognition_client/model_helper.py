from accounts.models import Account
from speaker_recognition_client.models import VoiceRecord

_SUCCESS_RETURN_STATUS = 'success'
_FAILURE_RETURN_STATUS = 'failure'


def create_record_from_request(request, account_id):
    account = Account.objects.get(pk=account_id)
    voice_record = VoiceRecord.objects.create(
        account=account,
        identification_profile_id=request['identificationProfileId']
    )

    if voice_record:
        return _SUCCESS_RETURN_STATUS
    else:
        return "Account does not exist"


def get_identification_profile_id(account_id):
    account = Account.objects.get(pk=account_id)
    voice_record = VoiceRecord.objects.get(account=account)

    if voice_record:
        return voice_record.identification_profile_id
    else:
        return "Account does not exist"


def update_enrollment_status_url(account_id, enrollment_status_url):
    account = Account.objects.get(pk=account_id)
    voice_record = VoiceRecord.objects.get(account=account)

    if not voice_record:
        return "Account does not exist"

    old_enroll_status_url = voice_record.enrollment_status_url

    voice_record.enrollment_status_url = enrollment_status_url
    voice_record.save()

    if voice_record.enrollment_status_url == old_enroll_status_url:
        return "Enrollment could not be updated"
    else:
        return _SUCCESS_RETURN_STATUS


def get_operation_status_url(identification_profile_id):
    voice_record = VoiceRecord.objects.get(pk=identification_profile_id)

    if voice_record:
        return _SUCCESS_RETURN_STATUS, voice_record.enrollment_status_url
    else:
        return _FAILURE_RETURN_STATUS, "Voice Record with identification_profile_id=" + \
               identification_profile_id + " does not exist"


def get_identification_profile_ids(account_ids_array):
    identification_profile_ids_array = []

    if len(account_ids_array) > 10:
        return "Cannot be given more than 10 accounts"
    elif len(account_ids_array) < 0:
        return "Cannot be given more than 0 accounts"
    else:
        for account_id in account_ids_array:
            identification_profile_id = get_identification_profile_id(account_id)
            identification_profile_ids_array.append(identification_profile_id)

        return identification_profile_ids_array


def get_identification_profile_ids_between(from_id, to_id):
    if not isinstance(from_id, int) or not isinstance(to_id, int):
        return "arg must be integer, found " + str(type(from_id))

    identification_profile_ids_array = []
    voice_records = VoiceRecord.objects.all()[from_id:to_id]

    for voice_record in voice_records:
        identification_profile_ids_array.append(voice_record.identification_profile_id)

    return identification_profile_ids_array


def get_objects_count():
    count = VoiceRecord.objects.count()

    return count


def get_account_from_identification_profile_id(identification_profile_id):
    voice_record = VoiceRecord.objects.get(pk=identification_profile_id)
    account = Account.objects.get(pk=voice_record.account_id)

    return account
