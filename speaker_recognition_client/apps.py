from __future__ import unicode_literals

from django.apps import AppConfig


class SpeakerRecognitionClientConfig(AppConfig):
    name = 'speaker_recognition_client'
