from django.conf.urls import url
from speaker_recognition_client import views

urlpatterns = [
    url(r'^voice-records/create-speaker-profile/(?P<account_id>\d+)$',  views.CreateProfileView.as_view()),
    url(r'^voice-records/enroll-speaker-profile/(?P<account_id>\d+)$',  views.EnrollProfileView.as_view()),
    url(r'^voice-records/get-operation-status/(?P<account_id>\d+)$',  views.GetOperationStatus.as_view()),
    url(r'^voice-records/reset-enrollments/(?P<account_id>\d+)$',  views.ResetEnrollmentsView.as_view()),
    url(r'^voice-records/identify-speaker$',  views.Identification.as_view()),
]

