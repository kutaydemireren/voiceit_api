from rest_framework import permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response
from speaker_recognition_client import client_base, model_helper, client_view_manager
from speaker_recognition_client.responses import EnrollmentResponse
from accounts.serializers import AccountSerializer

_OPERATION_LOCATION_HEADER = 'Operation-Location'
_STATUS_SUCCESS_RETURN = 'succeeded'
_STATUS_FAILURE_RETURN = 'failed'
_STATUS_NOT_STARTED_RETURN = 'notstarted'
_STATUS_RUNNING_RETURN = 'running'

_NOT_SUCCEED_ARRAY = [_STATUS_FAILURE_RETURN, _STATUS_NOT_STARTED_RETURN, _STATUS_RUNNING_RETURN]

_FILE_FIELD_NAME = 'file'
_SUCCESS_RETURN = 'success'
_FAILURE_RETURN = 'failure'
_SHORT_AUDIO_STATE = 'true'
_STATUS_FIELD_NAME = 'status'
_RESPONSE_FIELD_NAME = 'response'


class CreateProfileView(APIView):
    """
    Create a new user profile on Speaker Recognition API.

    Before identifying the voices, their creating profile and enrollment phase should be completed.

    **param account_id** VoiceIt API account_id

    **return Response** Success if created, error message if could not created
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, account_id, format=None):
        try:
            response_data = client_base.create_profile()
            response = model_helper.create_record_from_request(response_data.json(), account_id)

            return Response(response, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response(e, status=status.HTTP_400_BAD_REQUEST)


class EnrollProfileView(APIView):
    """
    Enroll given account's voice on Speaker Recognition API.

    Before identifying the voices, their creating profile and enrollment phase should be completed.

    Although this returns information about operation status, to check separately whether the process is done or not,
    you might call /get-operation-status/{account_id} endpoint.

    The audio file format must meet the following requirements: Container: WAV / \
    Encoding: PCM / Rate: 16K / Sample Format: 16 bit / Channels: Mono

|               |               |
|---------------|---------------|
| CONTAINER     | WAV           |
| ENCODING      | PCM           |
| RATE          | 16K           |
| SAMPLE FORMAT | 16 Bit        |
| CHANNELS      | MONO          |
|               |               |

    **param account_id** -- VoiceIt API account_id

    **return Response** -- Success if enroll, error message if could not enroll
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, account_id):
        file = request.data[_FILE_FIELD_NAME]

        response = client_view_manager.enroll_profile(account_id=account_id, file=file)

        return Response(response[_RESPONSE_FIELD_NAME], response[_STATUS_FIELD_NAME])


class GetOperationStatus(APIView):
    """
    Returns the given operation's status from Speaker Recognition API

    **param account_id** VoiceIt API account_id

    **return Response** operation status response of the Speaker Recognition API
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, account_id):
        identification_profile_id = model_helper.get_identification_profile_id(account_id)
        return_status, operation_status_url = model_helper.get_operation_status_url(identification_profile_id)
        if return_status == _FAILURE_RETURN:
            error_message = operation_status_url
            return Response(error_message, status=status.HTTP_404_NOT_FOUND)

        response = client_base.get_operation_status(operation_status_url)
        if response.ok:
            return Response(response.json(), status=status.HTTP_200_OK)
        else:
            return Response(response.json()['error']['message'], status=status.HTTP_400_BAD_REQUEST)


class ResetEnrollmentsView(APIView):
    """
    Deletes all enrollments associated with the given speaker identification profile permanently from the service.

    **param account_id** VoiceIt API account_id

    **return Response** success if reset, error message if something went wrong
    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, account_id, format=None):
        identification_profile_id = model_helper.get_identification_profile_id(account_id)
        response = client_base.reset_enrollments(identification_profile_id=identification_profile_id)

        if response.ok:
            return Response(_SUCCESS_RETURN, status=status.HTTP_200_OK)
        else:
            return Response(response.json()['error']['message'], status=status.HTTP_400_BAD_REQUEST)


class IdentifySpeakerView(APIView):
    """
    **Deprecated**

    Identifies the speaker's identity.

    Before identifying the voices, their creating profile and enrollment phase should be completed.

    Although this returns information about operation status, to check separately whether the process is done or not,
    you might call /get-operation-status/{account_id} endpoint.

    The audio file format must meet the following requirements: Container: WAV / \
    Encoding: PCM / Rate: 16K / Sample Format: 16 bit / Channels: Mono

|               |               |
|---------------|---------------|
| CONTAINER     | WAV           |
| ENCODING      | PCM           |
| RATE          | 16K           |
| SAMPLE FORMAT | 16 Bit        |
| CHANNELS      | MONO          |
|               |               |

    **return Response** -- Identified account if succeeded, error message if could not

    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            file = request.data[_FILE_FIELD_NAME]
            status_return, message_trace_or_proc = client_view_manager.identify_speaker(file=file)

            if status_return == _SUCCESS_RETURN:
                speaker_identification_id = message_trace_or_proc[0]
                confidence = message_trace_or_proc[1]
                account = model_helper.get_account_from_identification_profile_id(speaker_identification_id)

                serializer = AccountSerializer(account, context={'request': request})
                response = {'account': serializer.data,
                            'confidence': confidence}
                return Response(response, status=status.HTTP_200_OK)
            else:
                message_trace = message_trace_or_proc
                return Response({'errors': str(message_trace)}, status=status.HTTP_404_NOT_FOUND)
        except Exception as ex:
            error_message = {'error': 'Error has occured.',
                             'error_message': ex.__str__()
                             }
            return Response(error_message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Identification(APIView):
    """
    Identifies the speaker's identity.

    Before identifying the voices, their creating profile and enrollment phase should be completed.

    Although this returns information about operation status, to check separately whether the process is done or not,
    you might call /get-operation-status/{account_id} endpoint.

    The audio file format must meet the following requirements: Container: WAV / \
    Encoding: PCM / Rate: 16K / Sample Format: 16 bit / Channels: Mono

|               |               |
|---------------|---------------|
| CONTAINER     | WAV           |
| ENCODING      | PCM           |
| RATE          | 16K           |
| SAMPLE FORMAT | 16 Bit        |
| CHANNELS      | MONO          |
|               |               |

    **return Response** -- Identified account if succeeded, error message if could not

    """
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            response = client_view_manager.identification(request)

            return response
        except Exception as ex:
            error_message = {'error': 'Error has occured.',
                             'error_message': ex.__str__()
                             }
            return Response(error_message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
