from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND


def none_type_object(extra_error_inf=None):
    """
    :param extra_error_inf: can be used for details of the error, like where it happened.
    :return: none type object error
    """
    error_message = 'None type object'
    error_status = HTTP_500_INTERNAL_SERVER_ERROR
    return __create_error(error_message=error_message, error_status=error_status, extra_error_inf=extra_error_inf)


def custom_error(error_message, error_status, extra_error_inf=None):
    return __create_error(error_message=error_message, error_status=error_status, extra_error_inf=extra_error_inf)


def microsoft_error(response):
    error_message = response['error', 'message']
    error_status = HTTP_404_NOT_FOUND
    return __create_error(error_message=error_message, error_status=error_status)


def __create_error(error_message, error_status, extra_error_inf=None):
    if extra_error_inf is not None:
        return {'error_message': error_message,
                'error_status': error_status,
                'extra_error_inf': extra_error_inf}
    else:
        return {'error_message': error_message,
                'error_status': error_status}
