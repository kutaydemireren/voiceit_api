from django.db import models
from accounts.models import Account


class VoiceRecord(models.Model):
    """
        Base class for voice records
    """
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account')
    identification_profile_id = models.UUIDField(primary_key=True, blank=False)
    enrollment_status_url = models.CharField(max_length=100, default="")
