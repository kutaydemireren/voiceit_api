from rest_framework import serializers
from speaker_recognition_client.models import VoiceRecord


class VoiceRecordSerializer(serializers.ModelSerializer):
    account_id = serializers.IntegerField(write_only=True)
    identification_profile_id = serializers.CharField(read_only=False)

    class Meta:
        model = VoiceRecord
        fields = ('account_id', 'identification_profile_id')
