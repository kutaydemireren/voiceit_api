import requests
from voiceit.settings import OCP_APIM_SUBSCRIPTION_KEY_SPEAKER_RECOG as SUBSCRIPTION_KEY

_BASE_URI = 'https://api.projectoxford.ai'
_OPERATION_PROC_RES_FIELD_NAME = 'processingResult'
_OPERATION_STATUS_FIELD_NAME = 'status'
_STATUS_RUNNING_RETURN = 'running'
_STATUS_SUCCESS_RETURN = 'succeeded'
_STATUS_FAILURE_RETURN = 'failed'

_REPEAT_TIME = 3


def create_profile():
    headers = {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    body = '{' \
           '"locale":"en-us",' \
           '}'

    url = _BASE_URI + "/spid/v1.0/identificationProfiles"

    try:
        response = requests.post(url, data=body, headers=headers)
        return response
    except Exception as e:
        return "[Errno {0}] {1}".format(e.errno, e.strerror)


def enroll_profile(identification_profile_id, file):
    headers = {
        'Content-Type': 'audio/wav; samplerate=1600',
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    url = _BASE_URI + '/spid/v1.0/identificationProfiles/{}/enroll'.format(identification_profile_id)

    response = requests.post(url, data=file, headers=headers)

    return response


def get_operation_status(operation_status_url):
    headers = {
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    response = requests.get(operation_status_url, headers=headers)

    return response


def get_operation_proc_result(operation_status_url):
    headers = {
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    response = requests.get(operation_status_url, headers=headers)

    return response


def reset_enrollments(identification_profile_id):
    headers = {
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    body = '{' \
           '}'

    url = _BASE_URI + '/spid/v1.0/identificationProfiles/{}/reset'.format(identification_profile_id)

    response = requests.post(url, data=body, headers=headers)

    return response


def identification(identification_profile_ids, file, short_audio=True):
    headers = {
        'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    }

    url = _BASE_URI + '/spid/v1.0/identify?identificationProfileIds={}&shortAudio={}'.format(identification_profile_ids,
                                                                                             short_audio)
    files = {'file': file}

    response = requests.post(url, files=files, headers=headers)

    return response
